## Глубокое обучение на python
В этом репозитории располагаются тетрадки *jupyter* с материалами из книги <a href='https://www.ozon.ru/context/detail/id/145615583/v' > "Глубокое обучение на Python", Франсуа Шолле</a>.
Тетрадки выступают в роли интерактивных конспектов для лучшего усвоения материала.

<img src="img/1.jpg"
     alt="photo"
     style="float: left; margin-right: 10px;" />